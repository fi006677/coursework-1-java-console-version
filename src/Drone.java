package uk.ac.reading.fi006677;

/**
 * class for simple drone which can move around an arena
 */

public class Drone {
	
	/**
	 * construct drone
	 * @param posX		initial x position of drone
	 * @param posY		initial y position of drone
	 * @param dx		difference in position (movement)
	 * @param dy		difference in position (movement)
	 * @param dir		direction of drone
	 * @param newDir	next direction of the drone
	 */
	
	private int posX = 0;
	private int posY = 0;
	private Direction dir;
	private int dx, dy;
	private Direction newDir;
	
	public Drone(int i, int j, Direction d) {
		posX = i;
		posY = j;
		dir = d;
		dx = 1;
		dy = 1;
		
		
	}

	public String toString() {
		return "Drone is at " + posX + ", " + posY + ", travelling " + dir;
	}
	
	/**
	 * display the drone in the canvas
	 * @param c	the canvas used
	 */
	public void displayDrone(ConsoleCanvas c) {
		// call the showIt method in c to put a D where the drone is
		c.showIt((posY),(posX),'D'); // x and y positions are switched as it loads as a matrix
	}
	
	
	/**
	 * move the drones
	 * @param newPosX		new x position of drone after movement
	 * @param newPosY		new y position of drone after movement
	 */
	public void tryToMove(DroneArena da) {
		int newPosX = posX;
		int newPosY = posY;
		
		if (dir == Direction.North) {
			newPosY = posY - dy; // move the drone north
		}
		else if (dir == Direction.East){
			newPosX = posX + dx; // move the drone east
		}
		else if (dir == Direction.South){
			newPosY = posY + dy; // move the drone south
		}
		else if (dir == Direction.West){
			newPosX = posX - dx; // move the drone west
		}
		
		/**
		 * change to the next direction in the enum sequence
		 */
		if (dir == Direction.North) {
			newDir = Direction.East;
		}
		else if (dir == Direction.East){
			newDir = Direction.South;
		}
		else if (dir == Direction.South){
			newDir = Direction.West;
		}
		else if (dir == Direction.West){
			newDir = Direction.North;
		}
		
		
		
		
		/**
		 * checks if the drone can move to the next position, does so if it can, and changes direction if it can't
		 */
		switch (da.canMoveHere(newPosX, newPosY)) {
			case 0 : posX = newPosX;
					 posY = newPosY;
					 break;
			case 1 : 
					 dir = newDir;
					 break;
			case 2 : 
					 dir = newDir;
					 break;
			case 3 : 
					 dir = newDir;
					 break;
		}
	}
	
	
	public static void main(String[] args) {
		Drone d = new Drone(5, 3, Direction.South);		// creates drone
		System.out.println(d.toString());	// prints location
		Drone d2 = new Drone(4, 6, Direction.North);		// creates drone
		System.out.println(d2.toString());	// prints location
	}
	
	


}
