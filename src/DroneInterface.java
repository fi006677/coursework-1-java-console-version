package uk.ac.reading.fi006677;

import java.util.Scanner;
import java.io.File; 
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class to hold the interface
 */
public class DroneInterface {
	private Scanner s; // scanner used for input from user
	   private DroneArena myArena;	// arena in which drones are shown
	    /**
	    	 * constructor for DroneInterface
	    	 * sets up scanner used for input and the arena
	    	 * then has main loop allowing user to enter commands
	    	 * @param ch		char for the user input that allows you to select different options in the interface
	    	 * @param xVal		x value for the arena size, determined by the user
	    	 * @param yVal		y value for the arena size, determined by the user
	    	 * @param fileName	name of the file that the arena will be saved to or loaded from
	     */
	    public DroneInterface() {
	    	 s = new Scanner(System.in);			// set up scanner for user input
	    	 myArena = new DroneArena(20, 20);	// create arena of size 20*20
	    	   	 
	        char ch = ' ';
	        int xVal;
	        int yVal;
	        String fileName;
	        do {
	        	System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay arena, (M)ove drones, \n"
	        			+ " specify a (N)ew arena size, (S)ave the current arena, \n"
	        			+ " (L)oad an existing arena, or e(X)it > ");
	        	ch = s.next().charAt(0);
	        	s.nextLine();
	        	switch (ch) {
	    			case 'A' :
	    			case 'a' :
	        			myArena.addDrone();	// add a new drone to arena
	        			break;
	        		case 'I' :
	        		case 'i' :
	        			System.out.print(myArena.toString()); // prints information about the drone's location and direction
	            		break;
	        		case 'D' :
	        		case 'd' :
	        			doDisplay(); // displays the arena
	        			break;
	        		case 'M' :
	        		case 'm' :
	        			for (int i=0; i<10; i++) {
	        				myArena.moveAllDrones(myArena); // moves all drones 10 units in the direction they're heading in
		        			doDisplay(); // displays the arena
		        			System.out.print(myArena.toString()); // prints information about the drone's location and direction
		        			try {
								Thread.sleep(200); // delays the next movement by 200ms
							} catch (InterruptedException e) {								
								e.printStackTrace();
							}
	        			}
	        			break;	        		
	        		case 'N' :
	        		case 'n' :	        			
	    	        	System.out.print("Specify an X value for the size of the arena:");
	    	        	xVal = s.nextInt(); // saves the user's input as the X value
	    	        	s.nextLine();
	    	        	System.out.print("Specify an Y value for the size of the arena:");
	    	        	yVal = s.nextInt(); // saves the user's input as the Y value
	    	        	s.nextLine();
	    	        	myArena = new DroneArena(xVal, yVal); // redraws the arena with the custom sizes
	        			break;
	        		case 'S' :
	        		case 's' :
	        			System.out.print("Name your file (no spaces or punctuation): >");
	    	        	fileName = s.next(); // saves the user's input as the file name
	    	        	s.nextLine();
	    	        	try {
	    	        	      File saveArena = new File(fileName + ".txt"); //creates a new file
	    	        	      if (saveArena.createNewFile()) {
	    	        	    	  FileWriter myWriter = new FileWriter(saveArena);
	    	        	          myWriter.write(myArena.toString()); //writes information to the file
	    	        	          myWriter.close(); //closes the file
	    	        	          System.out.println("Arena saved.");
	    	        	      } else {
	    	        	        System.out.println("File already exists."); //prints an error if the file name is already taken
	    	        	      }
	    	        	    } catch (IOException e) {
	    	        	      System.out.println("An error occurred.");
	    	        	      e.printStackTrace();
	    	        	    }
	        			break;
	        		case 'L' :
	        		case 'l' :
	        			System.out.print("What file would you like to load? (Enter [name].txt) > ");
	    	        	fileName = s.next(); // saves the user's input as the file name
	    	        	s.nextLine();
	    	        	try {
	    	        	      File loadFile = new File(fileName);
	    	        	      Scanner loadedFile = new Scanner(loadFile);
	    	        	      while (loadedFile.hasNextLine()) {
	    	        	        String data = loadedFile.nextLine();
	    	        	        System.out.println(data);
	    	        	      }
	    	        	      loadedFile.close();
	    	        	    } catch (FileNotFoundException e) {
	    	        	      System.out.println("An error occurred.");
	    	        	      e.printStackTrace();
	    	        	    }
	        			break;
	        		case 'x' : 	ch = 'X';				// when X detected program ends
	        			break;
	        	}
	    		} while (ch != 'X');						// test if end
	        
	       s.close();									// close scanner
	    }
	    
		public static void main(String[] args) {
			DroneInterface r = new DroneInterface(); // calls the interface
			
		}
		
		/**
	    /**
	     * Display the drone arena on the console
	     * 
	     */
	    void doDisplay() {
	 		// determine the arena size
	    	int arenaX = myArena.getArenaX();
	    	int arenaY = myArena.getArenaY();
	    	// hence create a suitable sized ConsoleCanvas object
	    	ConsoleCanvas c = new ConsoleCanvas(arenaX, arenaY);
	    	// call showDrones suitably
	    	myArena.showDrones(c);
	    	// then use the ConsoleCanvas.toString method
	    	System.out.println(c.toString());
	    	
	    }
	    


}
