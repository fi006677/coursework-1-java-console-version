package uk.ac.reading.fi006677;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class to hold one or more drones, of type Drone initially
 */

public class DroneArena {
	
	/**
	 * construct arena
	 * @param arenaX		x size of arena
	 * @param arenaY		y size of arena
	 * @param posX			initial x position of drone
	 * @param posY			initial y position of drone
	 * @param dir			direction of drone
	 * @param manyDrones	arrayList of all the drones
	 */
	
	private int arenaX = 0;
	private int arenaY = 0;
	private int posX = 0;
	private int posY = 0;
	private Direction dir;
	private ArrayList<Drone> manyDrones= new ArrayList<Drone>();
	
	public DroneArena (int i, int j) {
		arenaX = i;
		arenaY = j;
	}
	
	public int getPosX() {
	    return posX;
	  }
	
	public int getPosY() {
	    return posY;
	  }
	
	/**
	 * generates a random number for the x and y values for the drone
	 * generates a random direction for it to start moving in
	 * adds the drone to the manyDrones ArrayList
	 */
	public void addDrone () {	
		Random randomGenerator;
		randomGenerator = new Random();	
		posX = randomGenerator.nextInt(getArenaX()-1) + 1;
		posY = randomGenerator.nextInt(getArenaY()-1) + 1;
		dir = Direction.getRandomDirection();
		Drone newDrone = new Drone(posX, posY, dir);
		manyDrones.add(newDrone);
		
	}
	
	public String toString() {
		for (int i = 0; i < manyDrones.size();i++) 
	      { 		      
	          System.out.println(manyDrones.get(i)); 		
	      }
		return "Size of arena is " + getArenaX() + ", " + getArenaY() + "\n";
	}
	
	

	
	public static void main(String[] args) {
		
		DroneArena a = new DroneArena(20, 10);	// create drone arena
		
		for (int ct = 0; ct < 3; ct++) {
			a.addDrone();
			System.out.println(a.toString());	// print where is
		}
		   
	}
	
	/**
	 * show all the drones in the interface
	 * @param c	the canvas in which drones are shown
	 */
	public void showDrones(ConsoleCanvas c) {
		// loop through all the Drones calling the displayDrone method
		for (Drone d : manyDrones) {
			d.displayDrone(c);
		}
	}
	
	/**
	 * moves all the drones in the interface
	 */
	public void moveAllDrones(DroneArena da) {
		for (Drone md : manyDrones) {
			md.tryToMove(da);
		}
	}

	public int getArenaX() {
		return arenaX;
	}


	public int getArenaY() {
		return arenaY;
	}
	
	/**
	 * determines whether or not the drone can move to the next location  
	 */
	public int canMoveHere(int x, int y) {
		int status = 0;
		if (x<1 || x>= arenaX-1) status += 1; // hits vertical wall
		if (y<1 || y>= arenaY-1) status += 2; // hits horizontal wall
		return status;
	}
	
	
}
