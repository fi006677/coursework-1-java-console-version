package uk.ac.reading.fi006677;

import java.util.Random;

/**
 *  Enum for each direction that the drones move in
 */

public enum Direction {
	North, East, South, West;
	
	/**
	 *  Picks a random direction for each drone to move in
	 */
	public static Direction getRandomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	public Direction nextDirection() {
		return values()[(ordinal()+1) % values().length];
	}
	
}
