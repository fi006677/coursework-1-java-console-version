package uk.ac.reading.fi006677;

/**
 *  Class to handle a canvas
 */

public class ConsoleCanvas {
	
	int i, j;
	String res;
	
	char[][] canvas;
	
	/**
	 *  prints the arena with the specified dimensions
	 */
	public ConsoleCanvas(int x, int y) {
		i = x;
		j = y;
		canvas = new char[i][j];
		
		for(int m=0; m < j;m++){
		    for(int n=0; n < i; n++){
		        if(m == 0||m == j-1){
		        	canvas[m][n] = '#';
		        }
		        else{
		            if(n == 0||n == i-1){
		            	canvas[m][n] = '#'; 
		            }
		            else{
		            	canvas[m][n] = ' '; 
		            }
		        }
		    }
		}
	}

	public void showIt(int m, int n, char d) {
		canvas[m][n] = d;
	}
	/**
	 *  prints the arena with the drones added
	 */
	public String toString()
    {
        String res = "";
        for(int m = 0; m<i; m++)
        {
            for(int n = 0; n<j; n++)
            {
                res = res + canvas[m][n] + " ";
            }
            res = res + "\n";
        }

        return res;
    }

	
	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas (20, 20);	// create a canvas
		c.showIt(4,3,'D');								// add a Drone at 4,3
		System.out.println(c.toString());				// display result
	}

	


	

}
